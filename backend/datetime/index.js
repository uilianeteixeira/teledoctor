const CalendarDate = require('./CalendarDate.model');
const CalendarTime = require('./CalendarTime.model');
const express = require('express');
const app = express();
const connectDb = require('./connection');
const PORT = 8083;

app.get('/calendarDate', async (req, res) => {
  const calendarDate = await CalendarDate.find();
  res.json(calendarDate);
});

app.get('/calendarTime', async (req, res) => {
  const calendarTime = await CalendarTime.find();
  res.json(calendarTime);
});

app.listen(PORT, function () {
  console.log(`Ouvindo na porta ${PORT}`);
  connectDb().then(() => {
    console.log('MongoDb conectado');
  });
});
