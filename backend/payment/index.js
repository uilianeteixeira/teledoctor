const Payment = require('./Payment.model');
const express = require('express');
const app = express();
const connectDb = require('./connection');
const PORT = 8086;

app.get('/payment-list', async (req, res) => {
  const payment = await Payment.find({payment: 'PENDENTE'});
  res.json(payment);
});

app.get('/payment-list-pago', async (req, res) => {
  const payment = await Payment.find({payment: 'PAGO'});
  res.json(payment);
});

app.put('/alter-payment/:id', async (req, res) => {
  try {
    const payment = await Payment.findByIdAndUpdate(req.params.id, {
      $set: {payment: 'PAGO'},
    });
    return res.send({payment});
  } catch (err) {
    return res.status(400).send({error: 'Falha ao alterar pagamento'});
  }
});

app.listen(PORT, function () {
  console.log(`Ouvindo na porta ${PORT}`);
  connectDb().then(() => {
    console.log('MongoDb conectado');
  });
});
