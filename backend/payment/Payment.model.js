const mongoose = require('mongoose');
const paymentSchema = new mongoose.Schema({
  payment: {
    type: String,
  },
});
const Payment = mongoose.model('Payment', paymentSchema, 'schedulings');
module.exports = Payment;
