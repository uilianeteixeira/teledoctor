const User = require('./User.model');
const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcryptjs');
const app = express();
const connectDb = require('./connection');
const PORT = 8085;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

//Cadastro de usuário não está sendo implementado no front-end
app.post('/register', async (req, res) => {
  try {
    const user = await User.create(req.body);
    user.password = undefined;
    return res.send({user});
  } catch (err) {
    return res.status(400).send({error: 'Falha ao registrar usuário'});
  }
});

app.post('/authentication', async (req, res) => {
  const {email, password} = req.body;
  const user = await User.findOne({email}).select('+password');
  if (!user) return res.status(400).send({error: 'Usuário não encontrado'});
  if (!(await bcrypt.compare(password, user.password)))
    return res.status(400).send({error: 'Senha inválida'});
  user.password = undefined;
  return res.send({user});
});

app.listen(PORT, function () {
  console.log(`Ouvindo na porta ${PORT}`);
  connectDb().then(() => {
    console.log('MongoDb connected');
  });
});
