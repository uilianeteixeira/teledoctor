const mongoose = require('mongoose');
const schedulingSchema = new mongoose.Schema({
  specialty: {
    type: String,
    required: true,
  },
  professional: {
    type: String,
    required: true,
  },
  date: {
    type: String,
    required: true,
  },
  time: {
    type: String,
    required: true,
  },
  payment: {
    type: String,
    required: true,
  },
  price: {
    type: String,
    required: true,
  },
});
const Scheduling = mongoose.model('Scheduling', schedulingSchema);
module.exports = Scheduling;
