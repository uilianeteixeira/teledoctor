const Scheduling = require('./Scheduling.model');
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const connectDb = require('./connection');
const PORT = 8084;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.post('/sheduling-create', async (req, res) => {
  try {
    const scheduling = await Scheduling.create(req.body);
    return res.send({scheduling});
  } catch (err) {
    return res.status(400).send({error: 'Falha ao criar agendamento'});
  }
});

app.listen(PORT, function () {
  console.log(`Ouvindo na porta ${PORT}`);
  connectDb().then(() => {
    console.log('MongoDb connected');
  });
});
