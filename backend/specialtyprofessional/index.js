const Specialty = require('./Specialty.model');
const Professional = require('./Professional.model');
const express = require('express');
const app = express();
const connectDb = require('./connection');
const PORT = 8082;

app.get('/specialty', async (req, res) => {
  const specialty = await Specialty.find();
  res.json(specialty);
});

app.get('/professional', async (req, res) => {
  const professional = await Professional.find();
  res.json(professional);
});

app.listen(PORT, function () {
  console.log(`Ouvindo na porta ${PORT}`);
  connectDb().then(() => {
    console.log('MongoDb conectado');
  });
});
