import React from 'react';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import styles from './style';

const Header = ({title, navigation}) => {
  var logout = async () => {
    navigation.navigate('TypeAccess');
  };

  return (
    <View style={styles.container}>
      <Image
        style={styles.logo}
        source={require('../../assets/images/LogoActionBar.png')}
      />
      <Text style={styles.title}>{title}</Text>
      <TouchableOpacity
        onPress={() => {
          logout();
        }}>
        <Image
          onp
          style={styles.logo}
          source={require('../../assets/images/Logout.png')}
        />
      </TouchableOpacity>
    </View>
  );
};

export default Header;
