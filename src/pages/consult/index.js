import React from 'react';
import {ScrollView, View} from 'react-native';
import Header from '../../components/header';
import Consultations from './components/consultations';
import styles from './style';

const Consult = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Header title={'Realize sua consulta'} navigation={navigation} />
      <ScrollView>
        <Consultations navigation={navigation} />
      </ScrollView>
    </View>
  );
};

export default Consult;
