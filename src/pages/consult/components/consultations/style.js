import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  box: {
    borderWidth: 0.5,
    borderColor: 'gray',
    borderRadius: 5,
    backgroundColor: '#fff',
    margin: 10,
    padding: 10,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingBottom: 10,
    paddingTop: 15,
  },
  textButtonConsult: {
    fontWeight: 'bold',
    fontSize: 15,
    color: '#000',
    textAlign: 'center',
  },
  buttonConsult: {
    alignItems: 'center',
    backgroundColor: '#a0d4e7',
    borderWidth: 2,
    borderColor: '#3eabd0',
    padding: 8,
    margin: 5,
    width: 200,
    alignSelf: 'center',
  },
  description: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 16,
  },
});

export default styles;
