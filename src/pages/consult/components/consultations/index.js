import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import styles from './style';

const apiUrl = 'http://127.0.0.1:8086';

const Consultations = ({navigation}) => {
  const [consultations, setConsultations] = useState([]);

  function time(item) {
    let dateTimeNow = new Date();
    let dateTimeConsult = new Date(
      item.date.split('/')[2],
      item.date.split('/')[1] - 1,
      item.date.split('/')[0],
      item.time.split(':')[0],
      item.time.split(':')[1],
      0,
    );
    var timeMissing =
      (dateTimeConsult.getTime() - dateTimeNow.getTime()) / 1000 / 60;
    if (
      dateTimeConsult.toDateString() == dateTimeNow.toDateString() &&
      timeMissing <= 10
    ) {
      return true;
    } else {
      return false;
    }
  }

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      axios
        .get(apiUrl + '/payment-list-pago')
        .then(response => {
          setConsultations(response.data);
        })
        .catch(error => {
          console.log('Erro get Payment', error);
        });
    });
    return unsubscribe;
  }, [navigation]);

  return (
    <View>
      {consultations.map((item, index) => {
        return (
          <View key={index} style={styles.box}>
            <Text style={styles.description}>{item.specialty}</Text>
            <Text style={styles.description}>{item.professional}</Text>
            <View style={styles.row}>
              <Text>Data: {item.date}</Text>
              <Text>Horário: {item.time}h</Text>
            </View>
            {time(item) ? (
              <TouchableOpacity
                style={styles.buttonConsult}
                onPress={() => {
                  navigation.navigate('VideoConference');
                }}>
                <Text style={styles.textButtonConsult}>ENTRAR NA CONSULTA</Text>
              </TouchableOpacity>
            ) : (
              <Text style={{textAlign: 'center', fontStyle: 'italic'}}>
                Sua consulta será liberada 10 minutos antes do horário e data
                agendada.
              </Text>
            )}
          </View>
        );
      })}
    </View>
  );
};

export default Consultations;
