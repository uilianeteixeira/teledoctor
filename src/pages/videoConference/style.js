import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#000',
  },
  buttonClose: {
    width: 40,
    height: 40,
    backgroundColor: 'gray',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewButtonClose: {
    alignItems: 'flex-end',
    padding: 15,
  },
  image: {
    marginTop: 40,
    height: 450,
  },
});

export default styles;
