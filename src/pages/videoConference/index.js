import React from 'react';
import {ImageBackground, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './style';

const VideoConference = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.viewButtonClose}>
        <TouchableOpacity
          style={styles.buttonClose}
          onPress={() => {
            navigation.navigate('Consulta');
          }}>
          <Icon name="close" color={'#fff'} size={25} />
        </TouchableOpacity>
      </View>
      <ImageBackground
        resizeMode={'contain'}
        style={styles.image}
        source={require('../../assets/images/video.jpg')}
      />
    </View>
  );
};

export default VideoConference;
