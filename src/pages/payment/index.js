import React from 'react';
import {ScrollView, View} from 'react-native';
import Header from '../../components/header';
import BoxPayment from './components/boxPayment';
import styles from './style';

const Payment = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Header title={'Pague sua consulta'} navigation={navigation} />
      <ScrollView>
        <BoxPayment navigation={navigation} />
      </ScrollView>
    </View>
  );
};

export default Payment;
