import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Alert, Text, TouchableOpacity, View} from 'react-native';
import {RadioButton} from 'react-native-paper';
import TextInputMask from 'react-native-text-input-mask';
import styles from './style';

const apiUrl = 'http://127.0.0.1:8086';

const BoxPayment = ({navigation}) => {
  const [visible, setVisible] = useState({visible: false, index: null});
  const [paymentOption, setPaymentOption] = useState(null);
  const [data, setData] = useState(null);
  const [pendingPayment, setpendingPayment] = useState([]);
  const [cartName, setCartName] = useState('');
  const [cartNumber, setCartNumber] = useState('');
  const [cartDate, setCartDate] = useState('');
  const [cartCod, setCartCod] = useState('');

  function getPayment() {
    axios
      .get(apiUrl + '/payment-list')
      .then(response => {
        setpendingPayment(response.data);
      })
      .catch(error => {
        console.log('Erro get Payment', error);
      });
  }

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getPayment();
    });
    return unsubscribe;
  }, [navigation]);

  return (
    <View>
      <Text style={styles.description}>
        Consultas agendadas com pagamento pendente:
      </Text>
      {pendingPayment.length > 0 ? (
        <>
          {pendingPayment.map((item, index) => (
            <View key={index}>
              <View
                style={
                  visible.visible && visible.index == index
                    ? [
                        styles.box,
                        {
                          borderRadius: 0,
                          borderTopLeftRadius: 5,
                          borderTopRightRadius: 5,
                        },
                      ]
                    : styles.box
                }>
                <View style={styles.viewInformations}>
                  <View style={styles.row}>
                    <Text>Especialista: </Text>
                    <Text>{item.specialty}</Text>
                  </View>
                  <View style={styles.row}>
                    <Text>Profissional: </Text>
                    <Text>{item.professional}</Text>
                  </View>
                  <View style={styles.row}>
                    <Text>Data: </Text>
                    <Text>{item.date}</Text>
                  </View>
                  <View style={styles.row}>
                    <Text>Horário: </Text>
                    <Text>{item.time}h</Text>
                  </View>
                  <View style={styles.row}>
                    <Text>Valor: </Text>
                    <Text>R$ {item.price},00</Text>
                  </View>
                </View>
                <View style={styles.viewPay}>
                  <TouchableOpacity
                    style={styles.buttonPay}
                    onPress={() => {
                      if (visible.visible && visible.index == index) {
                        Alert.alert('', 'Deseja cancelar o pagamento?', [
                          {
                            text: 'Não',
                            onPress: () => {},
                          },
                          {
                            text: 'Sim',
                            onPress: () => {
                              setVisible({visible: false, index: null});
                              setPaymentOption(null);
                            },
                          },
                        ]);
                      } else {
                        setVisible({visible: true, index: index});
                      }
                    }}>
                    <Text style={styles.textButtonPay}>
                      {visible.visible && visible.index == index
                        ? 'CANCELAR '
                        : 'PAGAR'}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              {visible.visible && visible.index == index && (
                <View style={styles.boxBottom}>
                  <Text>Escolher forma de pagamento: </Text>
                  <RadioButton.Group
                    onValueChange={newValue => {
                      setPaymentOption(newValue);
                    }}
                    value={paymentOption}>
                    <View style={styles.row}>
                      <RadioButton color="#0099cc" value={0} />
                      <Text>Boleto</Text>
                      <RadioButton color="#0099cc" value={1} />
                      <Text>Crédito</Text>
                    </View>
                  </RadioButton.Group>
                  {paymentOption === 0 && (
                    <TouchableOpacity
                      style={[
                        styles.buttonPay,
                        {width: 140, alignSelf: 'flex-end', marginRight: 0},
                      ]}
                      onPress={() => {
                        axios
                          .put(apiUrl + `/alter-payment/${item._id}`)
                          .then(response => {
                            Alert.alert(
                              'Simulação',
                              'Boleto Gerado e consulta PAGA!',
                            );
                            getPayment();
                          })
                          .catch(error => {
                            console.log('Erro Payment', error);
                          });
                      }}>
                      <Text style={styles.textButtonPay}>GERAR BOLETO</Text>
                    </TouchableOpacity>
                  )}
                  {paymentOption === 1 && (
                    <View>
                      <TextInputMask
                        style={styles.input}
                        placeholder="Nome impresso no cartão"
                        placeholderTextColor="gray"
                        onChangeText={value => {
                          setCartName(value);
                        }}
                        value={data}
                      />
                      <TextInputMask
                        style={styles.input}
                        placeholder="Número do cartão"
                        placeholderTextColor="gray"
                        onChangeText={value => {
                          setCartNumber(value);
                        }}
                        value={data}
                        keyboardType={'numeric'}
                        mask={'[0000] [0000] [0000] [0000]'}
                      />
                      <View
                        style={[styles.row, {justifyContent: 'space-between'}]}>
                        <TextInputMask
                          style={[styles.input, {width: '42%'}]}
                          placeholder="Data de validade"
                          placeholderTextColor="gray"
                          onChangeText={value => {
                            setCartDate(value);
                          }}
                          value={data}
                          keyboardType={'numeric'}
                          mask={'[00]{/}[00]'}
                        />
                        <TextInputMask
                          style={[styles.input, {width: '45%'}]}
                          placeholder="CVV"
                          placeholderTextColor="gray"
                          onChangeText={value => {
                            setCartCod(value);
                          }}
                          value={data}
                          keyboardType={'numeric'}
                          mask={'[000]'}
                        />
                      </View>
                      <TouchableOpacity
                        style={[
                          styles.buttonPay,
                          {
                            width: 180,
                            alignSelf: 'flex-end',
                            marginRight: 0,
                          },
                        ]}
                        onPress={() => {
                          if (
                            cartName != '' &&
                            cartNumber != '' &&
                            cartDate != '' &&
                            cartCod != ''
                          ) {
                            axios
                              .put(apiUrl + `/alter-payment/${item._id}`)
                              .then(response => {
                                Alert.alert(
                                  'Simulação',
                                  'Pago com o cartão de crédito.',
                                );
                                getPayment();
                              })
                              .catch(error => {
                                console.log('Erro Payment', error);
                              });
                          } else {
                            Alert.alert(
                              'Pagamento',
                              'Informe todos os dados do cartão de crédito.',
                            );
                          }
                        }}>
                        <Text style={styles.textButtonPay}>
                          PAGAR COM CARTÃO
                        </Text>
                      </TouchableOpacity>
                    </View>
                  )}
                </View>
              )}
            </View>
          ))}
        </>
      ) : (
        <Text style={{paddingLeft: 10}}>Nenhuma consulta encontrada</Text>
      )}
    </View>
  );
};

export default BoxPayment;
