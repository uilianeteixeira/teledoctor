import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  description: {
    padding: 10,
    fontWeight: 'bold',
    fontSize: 15,
    textAlign: 'center',
  },
  box: {
    borderWidth: 0.5,
    borderColor: 'gray',
    borderRadius: 5,
    flexDirection: 'row',
    backgroundColor: '#fff',
    marginTop: 10,
    marginLeft: 10,
    marginRight: 10,
    padding: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  viewInformations: {
    width: '65%',
  },
  viewPay: {
    width: '35%',
    justifyContent: 'center',
  },
  textButtonPay: {
    fontWeight: 'bold',
    fontSize: 15,
    color: '#000',
    textAlign: 'center',
  },
  buttonPay: {
    alignItems: 'center',
    backgroundColor: '#a0d4e7',
    borderWidth: 2,
    borderColor: '#3eabd0',
    padding: 8,
    margin: 10,
    width: 110,
    alignSelf: 'center',
  },
  boxBottom: {
    borderWidth: 0.5,
    borderColor: 'gray',
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    backgroundColor: '#fff',
    padding: 10,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
  },
  input: {
    padding: 5,
    margin: 10,
    fontSize: 14,
    borderWidth: 0.5,
    color: '#000',
  },
});

export default styles;
