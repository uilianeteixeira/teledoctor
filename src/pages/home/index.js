import React from 'react';
import {ImageBackground, ScrollView, Text, View} from 'react-native';
import Header from '../../components/header';
import styles from './style';

const Home = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Header title={'Seja Bem Vindo(a)'} navigation={navigation} />
      <ScrollView
        contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}>
        <View style={styles.row}>
          <ImageBackground
            resizeMode={'contain'}
            style={styles.imageName}
            source={require('../../assets/images/Logo.png')}
          />
          <ImageBackground
            resizeMode={'contain'}
            style={styles.image}
            source={require('../../assets/images/ImageAcesso.png')}
          />
        </View>
        <Text style={styles.description}>Aqui você encontra...</Text>
        <View style={[styles.viewStrip, {marginLeft: 20}]}>
          <Text style={styles.textStrip}>PROFISSIONAIS QUALIFICADOS</Text>
        </View>
        <View style={[styles.viewStrip, {marginLeft: 45}]}>
          <Text style={styles.textStrip}>PREÇO QUE CABE NO BOLSO</Text>
        </View>
        <View style={[styles.viewStrip, {marginLeft: 70}]}>
          <Text style={styles.textStrip}>E FACILIDADE DE ACESSO</Text>
        </View>
      </ScrollView>
    </View>
  );
};

export default Home;
