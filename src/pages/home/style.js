import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  image: {
    height: 270,
    width: 240,
    marginTop: 5,
  },
  imageName: {
    height: 160,
    width: 130,
    alignItems: 'flex-start',
    marginLeft: 10,
  },
  description: {
    marginLeft: 20,
    padding: 15,
    paddingLeft: 0,
    fontWeight: 'bold',
    fontSize: 16,
    color: '#77c0fc',
  },
  viewStrip: {
    backgroundColor: '#33add6',
    width: 270,
    margin: 8,
    padding: 10,
    borderLeftWidth: 5,
    borderLeftColor: '#2c19a0',
  },
  textStrip: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 15,
  },
});

export default styles;
