import axios from 'axios';
import React, {useState} from 'react';
import {Alert, ScrollView, Text, TouchableOpacity, View} from 'react-native';
import Header from '../../components/header';
import DateTime from './components/dateTime';
import SpecialtyDoctor from './components/specialtyDoctor';
import styles from './style';

const apiUrl = 'http://127.0.0.1:8084';

const Scheduling = ({navigation}) => {
  const [stage, setStage] = useState(0);
  const [data, setData] = useState({});
  const [selectedSpecialty, setSelectedSpecialty] = useState(0);
  const [selectedDoctor, setSelectedDoctor] = useState(0);
  const [date, setDate] = useState('');
  const [time, setTime] = useState('');

  const createScheduling = async () => {
    await axios.post(apiUrl + '/sheduling-create', {
      specialty: data.specialty,
      professional: data.doctor,
      date: data.data,
      time: data.time,
      payment: 'PENDENTE',
      price: data.price,
    });
    Alert.alert('Sucesso', 'Agendamento realizado!');
  };

  return (
    <View style={styles.container}>
      <Header title={'Agende sua consulta'} navigation={navigation} />
      <ScrollView>
        <SpecialtyDoctor
          stage={stage}
          setStage={setStage}
          data={data}
          setData={setData}
          selectedSpecialty={selectedSpecialty}
          setSelectedSpecialty={setSelectedSpecialty}
          selectedDoctor={selectedDoctor}
          setSelectedDoctor={setSelectedDoctor}
        />
        <DateTime
          stage={stage}
          setStage={setStage}
          data={data}
          setData={setData}
          date={date}
          setDate={setDate}
          time={time}
          setTime={setTime}
        />
        {stage >= 4 && (
          <View style={styles.row}>
            <TouchableOpacity
              style={styles.buttonCancel}
              onPress={() => {
                Alert.alert('', 'Deseja cancelar o agendamento?', [
                  {
                    text: 'Não',
                    onPress: () => {},
                  },
                  {
                    text: 'Sim',
                    onPress: () => {
                      setStage(0);
                      setSelectedDoctor(0);
                      setSelectedSpecialty(0);
                      setDate('');
                      setTime('');
                      navigation.navigate('Home');
                    },
                  },
                ]);
              }}>
              <Text style={styles.textButtonCancel}>CANCELAR</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.buttonSchedule}
              onPress={() => {
                setStage(0);
                setSelectedDoctor(0);
                setSelectedSpecialty(0);
                setDate('');
                setTime('');
                if (Object.keys(data).length == 5) {
                  createScheduling();
                } else {
                  Alert.alert('Aviso', 'Preencha todas as informações.');
                }
              }}>
              <Text style={styles.textButtonSchedule}>AGENDAR</Text>
            </TouchableOpacity>
          </View>
        )}
      </ScrollView>
    </View>
  );
};

export default Scheduling;
