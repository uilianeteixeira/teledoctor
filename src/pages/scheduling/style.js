import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
  },
  textButtonCancel: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#000',
  },
  buttonCancel: {
    alignItems: 'center',
    backgroundColor: '#c9c9c9',
    borderWidth: 2,
    borderColor: '#adadad',
    padding: 10,
    width: 140,
    alignSelf: 'center',
  },
  textButtonSchedule: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#fff',
  },
  buttonSchedule: {
    alignItems: 'center',
    backgroundColor: '#57bfe6',
    borderWidth: 2,
    borderColor: '#3eabd0',
    padding: 10,
    width: 140,
    alignSelf: 'center',
  },
});

export default styles;
