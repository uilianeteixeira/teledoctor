import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  description: {
    padding: 10,
  },
  picker: {
    backgroundColor: '#d8d8d8',
    borderWidth: 0.3,
    borderColor: 'gray',
    height: 30,
    justifyContent: 'center',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 5,
  },
  divider: {
    borderWidth: 0.5,
    borderColor: '#aeaeae',
    margin: 10,
  },
});

export default styles;
