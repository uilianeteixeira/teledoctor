import {Picker} from '@react-native-picker/picker';
import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Text, View} from 'react-native';
import {Divider} from 'react-native-paper';
import styles from './style';

const apiUrl = 'http://127.0.0.1:8082';

const SpecialtyDoctor = ({
  stage,
  setStage,
  data,
  setData,
  selectedSpecialty,
  setSelectedSpecialty,
  selectedDoctor,
  setSelectedDoctor,
}) => {
  const [doctorSpecialtie, setDoctorSpecialtie] = useState([]);
  const [specialty, setSpecialty] = useState([]);
  const [professional, setProfessional] = useState([]);

  useEffect(() => {
    axios
      .get(apiUrl + '/specialty')
      .then(response => {
        setSpecialty(response.data);
      })
      .catch(error => {
        console.log('Erro get Specialty', error);
      });
    axios
      .get(apiUrl + '/professional')
      .then(response => {
        setProfessional(response.data);
      })
      .catch(error => {
        console.log('Erro get Professional', error);
      });
  }, []);

  return (
    <View>
      <Text style={styles.description}>Escolha a especialidade:</Text>
      <View style={styles.picker}>
        <Picker
          selectedValue={selectedSpecialty}
          onValueChange={itemValue => {
            setDoctorSpecialtie(
              professional.filter(
                data =>
                  data.id_specialty == itemValue || data.id_specialty == 0,
              ),
            );
            setSelectedSpecialty(itemValue);
            if (itemValue != 0) {
              setStage(1);
              setData(
                Object.assign(data, {
                  specialty: specialty[itemValue].name,
                }),
              );
            } else {
              setStage(0);
              setSelectedDoctor(0);
            }
          }}>
          {specialty.map(item => {
            return (
              <Picker.Item
                key={item.id}
                label={item.name}
                value={item.id}
                color={item.id == 0 ? 'gray' : ''}
              />
            );
          })}
        </Picker>
      </View>
      <Divider style={styles.divider} />
      {stage >= 1 && (
        <View>
          <Text style={styles.description}>Escolha o profissional:</Text>
          <View style={styles.picker}>
            <Picker
              selectedValue={selectedDoctor}
              onValueChange={itemValue => {
                if (itemValue != 0 && typeof doctorSpecialtie != 'undefined') {
                  setSelectedDoctor(itemValue);
                  setStage(2);
                  setData(
                    Object.assign(data, {
                      doctor:
                        doctorSpecialtie[
                          itemValue > 1 ? itemValue - 1 : itemValue
                        ].name,
                    }),
                  );
                  setData(
                    Object.assign(data, {
                      price:
                        doctorSpecialtie[
                          itemValue > 1 ? itemValue - 1 : itemValue
                        ].price,
                    }),
                  );
                } else {
                  setStage(1);
                }
              }}>
              {doctorSpecialtie.map(item => (
                <Picker.Item
                  key={item.id}
                  label={item.name}
                  value={item.id}
                  color={item.id == 0 ? 'gray' : ''}
                />
              ))}
            </Picker>
          </View>
          <Divider style={styles.divider} />
          <View>
            {stage >= 2 ? (
              <Text style={{fontWeight: 'bold', paddingLeft: 10}}>
                O valor dessa consulta é R$ {data.price},00
              </Text>
            ) : (
              <></>
            )}
          </View>
          {stage >= 2 ? <Divider style={styles.divider} /> : <></>}
        </View>
      )}
    </View>
  );
};

export default SpecialtyDoctor;
