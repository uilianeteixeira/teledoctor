import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  description: {
    padding: 10,
  },
  divider: {
    borderWidth: 0.5,
    borderColor: '#aeaeae',
    margin: 10,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
  },
  input: {
    height: 30,
    margin: 15,
    height: 30,
    textAlign: 'center',
  },
  rowTime: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  modal: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    bottom: 0,
    position: 'absolute',
    height: '70%',
    backgroundColor: '#fff',
    width: '100%',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingLeft: 25,
    paddingRight: 25,
  },
});

export default styles;
