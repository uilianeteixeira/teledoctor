import axios from 'axios';
import React, {useEffect, useState} from 'react';
import {Alert, Modal, Text, TouchableOpacity, View} from 'react-native';
import {Calendar} from 'react-native-calendars';
import {Divider, RadioButton, TextInput} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {firstDay, lastDay} from '../../../../constants';
import styles from './style';

const DateTime = ({
  stage,
  setStage,
  data,
  setData,
  date,
  setDate,
  time,
  setTime,
}) => {
  const [visible, setVisible] = useState(false);
  const [markedDates, setMarkedDates] = useState({});
  const [daysAvailable, setDaysAvailable] = useState({});
  const [calendarDate, setCalendarDate] = useState([]);
  const [calendarTime, setCalendarTime] = useState([]);

  const apiUrl = 'http://127.0.0.1:8083';

  useEffect(() => {
    axios
      .get(apiUrl + '/calendarDate')
      .then(response => {
        setCalendarDate(response.data);
        const nowDate = new Date().getDate();
        var markedDates = {};
        var days = [];
        response.data.map(item => {
          var day = item.date.split('-')[2];
          if (nowDate < day) {
            var properties = {
              selected: false,
              color: 'green',
              textColor: 'white',
              endingDay: true,
              startingDay: true,
            };
            days.push(day);
            setDaysAvailable(days);
            var obj = {};
            obj[`${item.date}`] = properties;
            markedDates = Object.assign(markedDates, obj);
            setMarkedDates(markedDates);
          }
        });
      })
      .catch(error => {
        console.log('Erro get CalendarDate', error);
      });
    axios
      .get(apiUrl + '/calendarTime')
      .then(response => {
        setCalendarTime(response.data);
      })
      .catch(error => {
        console.log('Erro get CalendarTime', error);
      });
  }, []);
  return (
    <View>
      <Modal animationType={'slide'} transparent={true} visible={visible}>
        <View style={styles.modal}>
          <Calendar
            minDate={firstDay}
            maxDate={lastDay}
            hideExtraDays={true}
            firstDay={1}
            enableSwipeMonths={true}
            onDayPress={value => {
              let valueDate = value.dateString.toString().split('-');
              var day = value.dateString.toString().split('-')[2];
              let chosen = false;
              valueDate =
                valueDate[2] + '/' + valueDate[1] + '/' + valueDate[0];
              daysAvailable.map(item => {
                if (item == day) {
                  setDate(valueDate);
                  setData(
                    Object.assign(data, {
                      data: valueDate,
                    }),
                  );
                  setVisible(false);
                  chosen = true;
                  setStage(3);
                }
              });
              if (!chosen) {
                Alert.alert('', 'Escolha um dos dias que estão na cor verde');
              }
            }}
            markedDates={
              Object.values(markedDates).length > 0 ? markedDates : {}
            }
            markingType={'period'}
          />
        </View>
      </Modal>
      {stage >= 2 && (
        <View>
          <TouchableOpacity
            onPress={() =>
              Object.values(markedDates).length > 0
                ? setVisible(true)
                : Alert.alert(
                    'Vagas esgotadas!',
                    'Infelizmente não tem mais vagas para este mês.',
                  )
            }>
            <Text style={styles.description}>Escolha a data:</Text>
            <View
              style={[
                styles.row,
                {
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  height: 40,
                  padding: 0,
                  paddingLeft: 10,
                },
              ]}>
              <Icon name="calendar-month" size={25} />
              <TextInput
                editable={false}
                color="#aeaeae"
                style={styles.input}
                underlineColorAndroid="transparent"
                placeholder={
                  Object.values(markedDates).length > 0
                    ? 'Selecione uma data...'
                    : 'Vagas esgotadas!'
                }
                value={date}
              />
            </View>
          </TouchableOpacity>
          <Divider style={styles.divider} />
        </View>
      )}
      {stage >= 3 && (
        <View>
          <Text style={styles.description}>Escolha o horário:</Text>
          <RadioButton.Group
            onValueChange={newValue => {
              setTime(newValue);
              setData(
                Object.assign(data, {
                  time: calendarTime[0].time[newValue],
                }),
              );
              setStage(4);
            }}
            value={time}>
            {calendarTime.map(item => {
              return calendarDate.map((itemCalendar, index) => {
                var dateSelected = date.split('/')[0];
                var dateCalendar = itemCalendar.date.split('-')[2];
                if (dateSelected == dateCalendar) {
                  return (
                    <View key={index} style={styles.rowTime}>
                      <View key={index} style={styles.rowTime}>
                        {item.id_date == itemCalendar.id &&
                          date.length > 0 &&
                          item.time.map((time, index) => {
                            return (
                              <View key={index} style={styles.rowTime}>
                                <RadioButton color="#0099cc" value={index} />
                                <Text>{time}</Text>
                                <Text>h</Text>
                              </View>
                            );
                          })}
                      </View>
                    </View>
                  );
                }
              });
            })}
          </RadioButton.Group>
          <Divider style={styles.divider} />
        </View>
      )}
    </View>
  );
};

export default DateTime;
