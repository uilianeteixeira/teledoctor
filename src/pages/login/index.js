import axios from 'axios';
import React, {useEffect, useRef, useState} from 'react';
import {
  Alert,
  ImageBackground,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import styles from './style';

const apiUrl = 'http://127.0.0.1:8085';

const Login = ({navigation}) => {
  const [user, setUser] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const inputElementRef = useRef(null);

  useEffect(() => {
    inputElementRef.current.setNativeProps({
      style: {
        fontFamily: 'arial',
      },
    });
  }, []);

  validateLogin = async () => {
    setLoading(true);
    if (user != '' && password != '') {
      await axios
        .post(apiUrl + '/authentication', {email: user, password: password})
        .then(res => {
          setLoading(false);
          navigation.navigate('Home');
        })
        .catch(err => {
          console.log('erro login', err);
          setLoading(false);
          Alert.alert(
            'Ops...',
            'Não foi possível entrar, usuário ou senha inválidos.',
          );
          setUser('');
          setPassword('');
        });
    } else {
      Alert.alert('Ops...', 'Informe usuário e senha.');
      setLoading(false);
      setUser('');
      setPassword('');
    }
  };

  return (
    <ScrollView
      style={styles.container}
      contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}>
      <Spinner visible={loading} />
      <ImageBackground
        resizeMode={'contain'}
        style={styles.image}
        source={require('../../assets/images/ImageLogin.png')}
      />
      <ImageBackground
        resizeMode={'contain'}
        style={styles.name}
        source={require('../../assets/images/Logo.png')}
      />
      <TextInput
        autoCapitalize="none"
        style={styles.input}
        placeholder="Usuário"
        placeholderTextColor="#4cbde6"
        onChangeText={value => {
          setUser(value);
        }}
        value={user}
      />
      <TextInput
        ref={inputElementRef}
        autoCapitalize="none"
        style={styles.input}
        placeholder="Senha"
        secureTextEntry
        placeholderTextColor="#4cbde6"
        onChangeText={value => {
          setPassword(value);
        }}
        value={password}
      />
      <TouchableOpacity
        style={styles.buttonEnter}
        onPress={() => {
          validateLogin();
        }}>
        <Text style={styles.textButtonEnter}>ENTRAR</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.buttonBack}
        onPress={() => {
          navigation.navigate('TypeAccess');
        }}>
        <Text style={styles.textButtonBack}>VOLTAR</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

export default Login;
