import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: '100%',
  },
  image: {
    marginTop: 30,
    height: 200,
  },
  name: {
    height: 30,
    width: 200,
    alignSelf: 'center',
    margin: 15,
  },
  input: {
    padding: 15,
    margin: 15,
    backgroundColor: '#fff',
    fontWeight: 'bold',
    fontSize: 16,
    borderBottomWidth: 3,
    borderBottomColor: '#4cbde6',
    color: '#4cbde6',
  },
  textButtonEnter: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#fff',
  },
  buttonEnter: {
    alignItems: 'center',
    backgroundColor: '#a0d4e7',
    borderWidth: 2,
    borderColor: '#3eabd0',
    padding: 10,
    margin: 10,
    width: 140,
    alignSelf: 'center',
  },
  textButtonBack: {
    fontWeight: 'bold',
    fontSize: 16,
    color: '#85c5f9',
  },
  buttonBack: {
    alignItems: 'center',
    width: 140,
    margin: 20,
    alignSelf: 'center',
  },
});

export default styles;
