import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: '100%',
    justifyContent: 'center',
  },
  image: {
    marginTop: 10,
    height: 350,
  },
  name: {
    height: 30,
    width: 200,
    alignSelf: 'flex-end',
  },
  viewChoice: {
    paddingTop: 20,
    alignItems: 'center',
  },
  description: {
    fontWeight: 'bold',
    padding: 10,
    fontSize: 16,
  },
  textButton: {
    fontWeight: 'bold',
    fontSize: 14,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#a0d4e7',
    borderWidth: 2,
    borderColor: '#3eabd0',
    padding: 10,
    margin: 10,
    width: 140,
  },
});

export default styles;
