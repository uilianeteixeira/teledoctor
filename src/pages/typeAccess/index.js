import React from 'react';
import {ImageBackground, Text, TouchableOpacity, View} from 'react-native';
import styles from './style';

const TypeAcess = ({navigation}) => {
  return (
    <View style={styles.container}>
      <ImageBackground
        resizeMode={'contain'}
        style={styles.image}
        source={require('../../assets/images/ImageAcesso.png')}
      />
      <ImageBackground
        resizeMode={'contain'}
        style={styles.name}
        source={require('../../assets/images/Logo.png')}
      />
      <View style={styles.viewChoice}>
        <Text style={styles.description}>Escolha o tipo de acesso</Text>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            navigation.navigate('Login');
          }}>
          <Text style={styles.textButton}>PACIENTE</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => {
            navigation.navigate('Login');
          }}>
          <Text style={styles.textButton}>MÉDICO</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default TypeAcess;
