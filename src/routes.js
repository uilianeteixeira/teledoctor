import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Consult from './pages/consult';
import Home from './pages/home';
import Login from './pages/login';
import Payment from './pages/payment';
import Scheduling from './pages/scheduling';
import TypeAccess from './pages/typeAccess';
import VideoConference from './pages/videoConference';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function Tabs() {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        activeTintColor: '#fff',
        inactiveTintColor: 'gray',
        activeBackgroundColor: '#33b5e5',
        inactiveBackgroundColor: '#e0e0e0',
        labelStyle: {fontSize: 12, fontWeight: 'bold', paddingBottom: 5},
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: 'Início',
          tabBarIcon: ({color, focused}) => (
            <Icon name="home" color={color} size={focused ? 25 : 20} />
          ),
        }}
      />
      <Tab.Screen
        name="Scheduling"
        component={Scheduling}
        options={{
          tabBarLabel: 'Agendamento',
          tabBarIcon: ({color, focused}) => (
            <Icon
              name="calendar-month"
              color={color}
              size={focused ? 25 : 20}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Payment"
        component={Payment}
        options={{
          tabBarLabel: 'Pagamento',
          tabBarIcon: ({color, focused}) => (
            <Icon name="currency-usd" color={color} size={focused ? 25 : 20} />
          ),
        }}
      />
      <Tab.Screen
        name="Consulta"
        component={Consult}
        options={{
          tabBarLabel: 'Consulta',
          tabBarIcon: ({color, focused}) => (
            <Icon name="doctor" color={color} size={focused ? 25 : 20} />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

const Routes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="TypeAccess">
        <Stack.Screen
          options={{headerShown: false}}
          name="TypeAccess"
          component={TypeAccess}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Login"
          component={Login}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="Home"
          component={Tabs}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="VideoConference"
          component={VideoConference}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
